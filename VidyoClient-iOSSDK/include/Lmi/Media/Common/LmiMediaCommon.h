/**
{file:
	{name: LmiMediaCommon.h}
	{description: }
	{copyright:
		(c) 2007-2013 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
#ifndef LMI_MEDIACOMMON_H
#define LMI_MEDIACOMMON_H

#include <Lmi/Utils/LmiTypes.h>

LMI_BEGIN_EXTERN_C

/**
{package:
	{name: MediaCommon}
	{parent: Lmi}
	{include: Lmi/Media/Common/LmiMediaCommon.h}
	{library: LmiMediaCommon}
	{description: }
}
*/

LmiBool LmiMediaCommonInitialize(void);
void LmiMediaCommonUninitialize(void);

LMI_END_EXTERN_C

#if LMI_INLINE_NEED_HEADER_FILE_DEFINITIONS
#include <Lmi/Media/Common/LmiMediaCommonInline.h>
#endif

#endif
