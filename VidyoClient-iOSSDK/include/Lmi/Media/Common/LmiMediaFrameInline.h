/**
{file:
	{name: LmiMediaFrameInline.h}
	{description: }
	{copyright:
		(c) 2012-2015 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
LMI_INLINE LmiMediaType LmiMediaFrameImplGetType(const LmiMediaFrameImpl *f)
{
	return f->mediaType;
}

LMI_INLINE const LmiMediaFormat *LmiMediaFrameImplGetFormat(const LmiMediaFrameImpl *f)
{
	return &f->format;
}

LMI_INLINE void LmiMediaFrameImplSetBuffer(LmiMediaFrameImpl *f, void *buffer)
{
	f->buffer = buffer;
}

LMI_INLINE LmiSizeT LmiMediaFrameImplGetSize(const LmiMediaFrameImpl *f)
{
	return f->size;
}

LMI_INLINE LmiAllocator *LmiMediaFrameImplGetAllocator(const LmiMediaFrameImpl *f)
{
	return f->alloc;
}

LMI_INLINE LmiAllocator *LmiMediaFrameImplGetBufferAllocator(const LmiMediaFrameImpl *f)
{
	return f->bufferAlloc != NULL ? f->bufferAlloc : f->alloc;
}

LMI_INLINE const LmiProperties *LmiMediaFrameImplGetPropertiesConst(const LmiMediaFrameImpl *f)
{
	return &f->properties;
}

LMI_INLINE LmiProperties *LmiMediaFrameImplGetProperties(LmiMediaFrameImpl *f)
{
	return &f->properties;
}

LMI_INLINE LmiMediaFrameImpl *LmiMediaFrameGetImpl(const LmiMediaFrame *f)
{
	return LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl);
}

LMI_INLINE LmiMediaType LmiMediaFrameGetType(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetType(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE const char *LmiMediaFrameGetTypeCStr(const LmiMediaFrame *f)
{
	return LmiMediaTypeToCStr(LmiMediaFrameGetType(f));
}

LMI_INLINE const LmiMediaFormat *LmiMediaFrameGetFormat(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetFormat(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE const void *LmiMediaFrameGetBuffer(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetBuffer(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE void LmiMediaFrameSetBuffer(LmiMediaFrame *f, void *frame)
{
	LmiMediaFrameImplSetBuffer(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl), frame);
}

LMI_INLINE LmiSizeT LmiMediaFrameGetSize(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetSize(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE LmiAllocator *LmiMediaFrameGetAllocator(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetAllocator(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE LmiAllocator *LmiMediaFrameGetBufferAllocator(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetBufferAllocator(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE const LmiProperties *LmiMediaFrameGetPropertiesConst(const LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetPropertiesConst(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE LmiProperties *LmiMediaFrameGetProperties(LmiMediaFrame *f)
{
	return LmiMediaFrameImplGetProperties(LmiSharedPtrGet(LmiMediaFrameImpl)(&f->impl));
}

LMI_INLINE LmiBool LmiMediaFrameIsExclusive(LmiMediaFrame *f)
{
	return LmiSharedPtrUnique(LmiMediaFrameImpl)(&f->impl);
}

