/**
{file:
	{name: LmiMediaMinMaxTracker.h}
	{description: }
	{copyright:
		(c) 2012 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
#ifndef LMI_MEDIAMINMAXTRACKER_H
#define LMI_MEDIAMINMAXTRACKER_H

#include <Lmi/Utils/LmiTypes.h>
#include <Lmi/Utils/LmiAllocator.h>
#include <Lmi/Os/LmiTime.h>

LMI_BEGIN_EXTERN_C

/**
{type visibility="private":
	{name: LmiMediaMinMaxTracker}
	{parent: MediaCommon}
	{include: Lmi/Media/Common/LmiMediaMinMaxTracker.h}
	{description: Tracks the minimum and maximum values.}
}
*/
typedef struct
{
	LmiAllocator *alloc;
	LmiTime trackPeriod;
	LmiTime lastSampleTime;
	LmiInt64 maxFirstHalfPeriod;
	LmiInt64 maxSecondHalfPeriod;
	LmiInt64 minFirstHalfPeriod;
	LmiInt64 minSecondHalfPeriod;
	LmiTime jitter;
	LmiTime largeCounter;
	LmiTime largeJitter;
} LmiMediaMinMaxTracker;

LmiMediaMinMaxTracker *LmiMediaMinMaxTrackerConstruct(LmiMediaMinMaxTracker *t, LmiTime trackerPeriod, LmiAllocator *a);
LMI_INLINE_DECLARATION void LmiMediaMinMaxTrackerDestruct(LmiMediaMinMaxTracker *x);
LmiMediaMinMaxTracker *LmiMediaMinMaxTrackerConstructCopy(LmiMediaMinMaxTracker *d, const LmiMediaMinMaxTracker *s);
LmiMediaMinMaxTracker *LmiMediaMinMaxTrackerAssign(LmiMediaMinMaxTracker *d, const LmiMediaMinMaxTracker *s);
LmiBool LmiMediaMinMaxTrackerAddSample(LmiMediaMinMaxTracker *t, LmiTime now, LmiInt64 sample);
LMI_INLINE_DECLARATION LmiInt64 LmiMediaMinMaxTrackerGetMin(LmiMediaMinMaxTracker *t);
LMI_INLINE_DECLARATION LmiInt64 LmiMediaMinMaxTrackerGetMax(LmiMediaMinMaxTracker *t);
LMI_INLINE_DECLARATION LmiTime LmiMediaMinMaxTrackerGetJitter(LmiMediaMinMaxTracker *t);

LMI_END_EXTERN_C

#if LMI_INLINE_NEED_HEADER_FILE_DEFINITIONS
#include <Lmi/Media/Common/LmiMediaMinMaxTrackerInline.h>
#endif

#endif
