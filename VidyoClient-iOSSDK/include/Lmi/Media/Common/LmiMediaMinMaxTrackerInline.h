/**
{file:
	{name: LmiMediaMinMaxTrackerInline.h}
	{description: }
	{copyright:
		(c) 2012 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
LMI_INLINE void LmiMediaMinMaxTrackerDestruct(LmiMediaMinMaxTracker *x)
{
}

LMI_INLINE LmiInt64 LmiMediaMinMaxTrackerGetMin(LmiMediaMinMaxTracker *t)
{
	return t->minFirstHalfPeriod < t->minSecondHalfPeriod ? t->minFirstHalfPeriod : t->minSecondHalfPeriod;
}

LMI_INLINE LmiInt64 LmiMediaMinMaxTrackerGetMax(LmiMediaMinMaxTracker *t)
{
	return t->maxFirstHalfPeriod > t->maxSecondHalfPeriod ? t->maxFirstHalfPeriod : t->maxSecondHalfPeriod;
}

LMI_INLINE LmiTime LmiMediaMinMaxTrackerGetJitter(LmiMediaMinMaxTracker *t)
{
	return t->jitter;
}
