/**
{file:
	{name: LmiRiffChunk.h}
	{description: }
	{copyright:
		(c) 2012 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
#ifndef LMI_RIFFCHUNK_H
#define LMI_RIFFCHUNK_H

#include <Lmi/Utils/LmiTypes.h>
#include <Lmi/Utils/LmiAllocator.h>
#include <Lmi/Utils/LmiPointerVector.h>
#include <Lmi/Os/LmiFile.h>

LMI_BEGIN_EXTERN_C

typedef struct LmiRiffChunk_
{
	char name[5];
	char listType[5];
	LmiFilePosition length;
	LmiFilePosition dataOffset;
	LmiPointerVector subchunks;
} LmiRiffChunk;

LmiRiffChunk *LmiRiffChunkConstruct(LmiRiffChunk *c, LmiFile *file, LmiAllocator *alloc);
void LmiRiffChunkDestruct(LmiRiffChunk *c);
LmiRiffChunk *LmiRiffChunkGetSubchunkByName(LmiRiffChunk *c, const char *name);
LmiRiffChunk *LmiRiffChunkGetSublistByName(LmiRiffChunk *c, const char *name);

LMI_END_EXTERN_C

#endif
