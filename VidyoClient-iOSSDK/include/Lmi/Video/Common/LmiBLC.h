/**
{file:
	{name: LmiBLC.h}
	{description: Defines the main structures and functions related to BLC.}
	{copyright:
		(c) 2010-2015 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/


#ifndef LMI_BLC_H
#define LMI_BLC_H


#include <Lmi/Video/Common/LmiVideoFrame.h>

LMI_BEGIN_EXTERN_C

#define LMI_BLC_UseHysteresis
#define LMI_BLC_MaxNumberOfBins               64
#define LMI_BLC_WeightMax                     128

/**
{type visibility="public":
	{name: LmiBLC}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiBLC.h}
	{description: LmiBLC object.}
}
*/
typedef struct {
	LmiUint bins[LMI_BLC_MaxNumberOfBins]; //intensity bins of 64
	LmiUint centerBins[LMI_BLC_MaxNumberOfBins]; //intensity bins of 64
	LmiUint lowThresh;
	LmiUint highThresh;
	LmiSizeT totalNumberOfSamplesInBins;
	LmiSizeT totalNumberOfSamplesInCenterBins;
	LmiSizeT binRefreshThresh;

	LmiSizeT imageWidth;
	LmiSizeT imageHeight;
	LmiSizeT topSectionPixelHeight;
	LmiSizeT bottomSectionPixelHeight;
	LmiSizeT leftSectionPixelWidth;
	LmiSizeT rightSectionPixelWidth;
	LmiUint avgPixelValue[5];
	LmiInt blcThresh;

	LmiBool blPicture;

} LmiBLC;

/**
{function visibility="public":
	{name: LmiBLCConstruct}
	{parent: LmiBLC}
	{description: Construct LmiBLC object.}
	{prototype: LmiBLC *LmiBLCConstruct(LmiBLC *x, LmiAllocator *a)}
	{parameter: {name: x} {description: Destination LmiBLC object.}}
	{parameter: {name: a} {description: LmiAllocator object.}}
	{return: Constructed LmiBLC object.}
}
*/
LmiBLC *LmiBLCConstruct(LmiBLC *x, LmiAllocator *a);

/**
{function visibility="public":
	{name: LmiBLCDestruct}
	{parent: LmiBLC}
	{description: Destruct LmiBLC object.}
	{prototype: void LmiBLCDestruct(LmiBLC *x)}
	{parameter: {name: x} {description: LmiBLC object.}}
}
*/
void LmiBLCDestruct(LmiBLC *x);

/**
{function visibility="public":
	{name: LmiBLCDetectAndCompensate}
	{parent: LmiBLC}
	{description: Detect Back light problems in the input picture and compensate to output.}
	{prototype: LmiBool LmiBLCDetectAndCompensate(LmiBLC *x, const LmiUint8 *in, LmiSizeT pitchIn, LmiUint8 *out, LmiSizeT pitchOut, LmiSizeT width, LmiSizeT height)}
	{parameter: {name: x} {description: LmiBLC object.}}
	{parameter: {name: in} {description: input samples to be BackLightCommpensated.}}
	{parameter: {name: pitchIn} {description: pitch of the input samples.}}
	{parameter: {name: out} {description: output compensated samples.}}
	{parameter: {name: pitchOut} {description: pitch of output samples.}}
	{parameter: {name: width} {description: width of the samples to be BackLightCommpensated.}}
	{parameter: {name: height} {description: height of the samples to be BackLightCommpensated.}}
	{return: LMI_TRUE when the picture has been compensated.}
}
*/
LmiBool LmiBLCDetectAndCompensate(LmiBLC *x, const LmiUint8 *in, LmiSizeT pitchIn, LmiUint8 *out, LmiSizeT pitchOut, LmiSizeT width, LmiSizeT height);

/**
{function visibility="public":
	{name: LmiBLCDetectAndCompensateVideoFrame}
	{parent: LmiBLC}
	{description: Detect Back light problems in the picture and compensate and output image.}
	{prototype: LmiBool LmiBLCDetectAndCompensateVideoFrame(LmiBLC *x, const LmiVideoFrame *inputPicture, LmiVideoFrame *outputPicture)}
	{parameter: {name: x} {description: LmiBLC object.}}
	{parameter: {name: inputPicture} {description: I420 frame to be BackLightCommpensated.}}
	{parameter: {name: outputPicture} {description: compensated picture}}
	{return: LMI_TRUE when the picture has been compensated.}
}
*/
LmiBool LmiBLCDetectAndCompensateVideoFrame(LmiBLC *x, const LmiVideoFrame *inputPicture, LmiVideoFrame *outputPicture);

/**
{function visibility="public":
	{name: LmiBLCDetectAndCompensateInVideoFrame}
	{parent: LmiBLC}
	{description: Detect Back light problems in the input picture and compensate the picture.}
	{prototype: LmiBool LmiBLCDetectAndCompensateInVideoFrame(LmiBLC *x, LmiVideoFrame *inputPicture)}
	{parameter: {name: x} {description: LmiBLC object.}}
	{parameter: {name: inputPicture} {description: I420 frame to be BackLightCommpensated.}}
	{return: LMI_TRUE when the picture has been compensated.}
}
*/

LmiBool LmiBLCDetectAndCompensateInVideoFrame(LmiBLC *x, LmiVideoFrame *inputPicture);

LMI_END_EXTERN_C

#endif // LMI_BLC_H
