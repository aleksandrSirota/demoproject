/**
{file:
	{name: LmiCameraControl.h}
	{description: }
	{copyright:
		(c) 2006-2019 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
#ifndef LMI_CAMERACONTROL_H
#define LMI_CAMERACONTROL_H

#include <Lmi/Utils/LmiTypes.h>
#include <Lmi/Utils/LmiAllocator.h>

LMI_BEGIN_EXTERN_C

/**
{package:
	{name: CameraControl}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiCameraControl.h}
	{description: #}
}
*/

/**
{type visibility="public":
	{name: LmiCameraControlParameter}
	{parent: CameraControl}
	{include: Lmi/Video/Common/LmiCameraControl.h}
	{description: #}
}
*/
typedef struct
{
	LmiBool hasFit;
	LmiBool hasDrive;
	LmiBool hasNudge;
} LmiCameraControlParameter;

/**
{function:
	{name: LmiCameraControlParameterConstructDefault}
	{parent: LmiCameraControlParameter}
	{description: Constructs an LmiCameraControlParameter object with no capabilities.}
	{prototype: LmiCameraControlParameter *LmiCameraControlParameterConstructDefault(LmiCameraControlParameter *p, LmiAllocator *alloc)}
	{parameter: {name: p} {description: The LmiCameraControlParameter object to construct.}}
	{parameter: {name: alloc} {description: The allocator to use.}}
	{return: A pointer to a constructed object on success, or NULL on failure.}
}
*/
LmiCameraControlParameter *LmiCameraControlParameterConstructDefault(LmiCameraControlParameter *p, LmiAllocator *alloc);

/**
{function:
	{name: LmiCameraControlParameterDestruct}
	{parent: LmiCameraControlParameter}
	{description: Destructs an LmiCameraControlParameter object.}
	{prototype: void LmiCameraControlParameterDestruct(LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: The LmiCameraControlParameter object to destruct.}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlParameterDestruct(LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlParameterAssign}
	{parent: LmiCameraControlParameter}
	{description: Assigns one LmiCameraControlParameter object the value of another.}
	{prototype: LmiCameraControlParameter *LmiCameraControlParameterAssign(LmiCameraControlParameter *p, const LmiCameraControlParameter *other)}
	{parameter: {name: p} {description: The LmiCameraControlParameter object to assign to.}}
	{parameter: {name: other} {description: The LmiCameraControlParameter object to make a copy of.}}
	{return: A pointer to the destination object on success, or NULL on failure.}
}
*/
LMI_INLINE_DECLARATION LmiCameraControlParameter *LmiCameraControlParameterAssign(LmiCameraControlParameter *p, const LmiCameraControlParameter *other);

/**
{function:
	{name: LmiCameraControlParameterEqual}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: LmiBool LmiCameraControlParameterEqual(const LmiCameraControlParameter *p, const LmiCameraControlParameter *other)}
	{parameter: {name: p} {description: #}}
	{parameter: {name: other} {description: #}}
	{return: #}
}
*/
LmiBool LmiCameraControlParameterEqual(const LmiCameraControlParameter *p, const LmiCameraControlParameter *other);

/**
{function:
	{name: LmiCameraControlParameterSetFit}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: void LmiCameraControlParameterSetFit(LmiCameraControlParameter *p, LmiBool enable)}
	{parameter: {name: p} {description: #}}
	{parameter: {name: enable} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlParameterSetFit(LmiCameraControlParameter *p, LmiBool enable);

/**
{function:
	{name: LmiCameraControlParameterSetDrive}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: void LmiCameraControlParameterSetDrive(LmiCameraControlParameter *p, LmiBool enable)}
	{parameter: {name: p} {description: #}}
	{parameter: {name: enable} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlParameterSetDrive(LmiCameraControlParameter *p, LmiBool enable);

/**
{function:
	{name: LmiCameraControlParameterSetNudge}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: void LmiCameraControlParameterSetNudge(LmiCameraControlParameter *p, LmiBool enable)}
	{parameter: {name: p} {description: #}}
	{parameter: {name: enable} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlParameterSetNudge(LmiCameraControlParameter *p, LmiBool enable);

/**
{function:
	{name: LmiCameraControlParameterClear}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: void LmiCameraControlParameterClear(LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: #}}
}
*/
void LmiCameraControlParameterClear(LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlParameterHasFit}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: LmiBool LmiCameraControlParameterHasFit(const LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION LmiBool LmiCameraControlParameterHasFit(const LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlParameterHasDrive}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: LmiBool LmiCameraControlParameterHasDrive(const LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION LmiBool LmiCameraControlParameterHasDrive(const LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlParameterHasNudge}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: LmiBool LmiCameraControlParameterHasNudge(const LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION LmiBool LmiCameraControlParameterHasNudge(const LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlParameterHasAny}
	{parent: LmiCameraControlParameter}
	{description: #}
	{prototype: LmiBool LmiCameraControlParameterHasAny(const LmiCameraControlParameter *p)}
	{parameter: {name: p} {description: #}}
	{return: #}
}
*/
LmiBool LmiCameraControlParameterHasAny(const LmiCameraControlParameter *p);

/**
{type visibility="public":
	{name: LmiCameraControlCapabilities}
	{parent: CameraControl}
	{include: Lmi/Video/Common/LmiCameraControl.h}
	{description: #}
}
*/
typedef struct
{
	LmiCameraControlParameter panTiltCaps;
	LmiCameraControlParameter zoomCaps;
	LmiBool hasPhotoCapture;
} LmiCameraControlCapabilities;

/**
{function:
	{name: LmiCameraControlCapabilitiesConstructDefault}
	{parent: LmiCameraControlCapabilities}
	{description: Constructs an LmiCameraControlCapabilities object with no capabilities.}
	{prototype: LmiCameraControlCapabilities *LmiCameraControlCapabilitiesConstructDefault(LmiCameraControlCapabilities *caps, LmiAllocator *alloc)}
	{parameter: {name: caps} {description: The LmiCameraControlCapabilities object to construct.}}
	{parameter: {name: alloc} {description: The allocator to use.}}
	{return: A pointer to a constructed object on success, or NULL on failure.}
}
*/
LmiCameraControlCapabilities *LmiCameraControlCapabilitiesConstructDefault(LmiCameraControlCapabilities *caps, LmiAllocator *alloc);

/**
{function:
	{name: LmiCameraControlCapabilitiesDestruct}
	{parent: LmiCameraControlCapabilities}
	{description: Destructs an LmiCameraControlCapabilities object.}
	{prototype: void LmiCameraControlCapabilitiesDestruct(LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: The LmiCameraControlCapabilities object to destruct.}}
}
*/
void LmiCameraControlCapabilitiesDestruct(LmiCameraControlCapabilities *caps);

/**
{function:
	{name: LmiCameraControlCapabilitiesAssign}
	{parent: LmiCameraControlCapabilities}
	{description: Assigns one LmiCameraControlCapabilities object the value of another.}
	{prototype: LmiCameraControlCapabilities *LmiCameraControlCapabilitiesAssign(LmiCameraControlCapabilities *caps, const LmiCameraControlCapabilities *other)}
	{parameter: {name: caps} {description: The LmiCameraControlCapabilities object to assign to.}}
	{parameter: {name: other} {description: The LmiCameraControlCapabilities object to make a copy of.}}
	{return: A pointer to the destination object on success, or NULL on failure.}
}
*/
LmiCameraControlCapabilities *LmiCameraControlCapabilitiesAssign(LmiCameraControlCapabilities *caps, const LmiCameraControlCapabilities *other);

/**
{function:
	{name: LmiCameraControlCapabilitiesEqual}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: LmiBool LmiCameraControlCapabilitiesEqual(const LmiCameraControlCapabilities *caps, const LmiCameraControlCapabilities *other)}
	{parameter: {name: caps} {description: #}}
	{parameter: {name: other} {description: #}}
	{return: #}
}
*/
LmiBool LmiCameraControlCapabilitiesEqual(const LmiCameraControlCapabilities *caps, const LmiCameraControlCapabilities *other);

/**
{function:
	{name: LmiCameraControlCapabilitiesSetPanTiltCaps}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: void LmiCameraControlCapabilitiesSetPanTiltCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p)}
	{parameter: {name: caps} {description: #}}
	{parameter: {name: p} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlCapabilitiesSetPanTiltCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlCapabilitiesSetZoomCaps}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: void LmiCameraControlCapabilitiesSetZoomCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p)}
	{parameter: {name: caps} {description: #}}
	{parameter: {name: p} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlCapabilitiesSetZoomCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p);

/**
{function:
	{name: LmiCameraControlCapabilitiesSetPhotoCapture}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: void LmiCameraControlCapabilitiesSetPhotoCapture(LmiCameraControlCapabilities *caps, LmiBool enable)}
	{parameter: {name: caps} {description: #}}
	{parameter: {name: enable} {description: #}}
}
*/
LMI_INLINE_DECLARATION void LmiCameraControlCapabilitiesSetPhotoCapture(LmiCameraControlCapabilities *caps, LmiBool enable);

/**
{function:
	{name: LmiCameraControlCapabilitiesClear}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: void LmiCameraControlCapabilitiesClear(LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: #}}
}
*/
void LmiCameraControlCapabilitiesClear(LmiCameraControlCapabilities *caps);

/**
{function:
	{name: LmiCameraControlCapabilitiesGetPanTiltCaps}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetPanTiltCaps(const LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetPanTiltCaps(const LmiCameraControlCapabilities *caps);

/**
{function:
	{name: LmiCameraControlCapabilitiesGetZoomCaps}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetZoomCaps(const LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetZoomCaps(const LmiCameraControlCapabilities *caps);

/**
{function:
	{name: LmiCameraControlCapabilitiesHasPhotoCapture}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: LmiBool LmiCameraControlCapabilitiesHasPhotoCapture(const LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: #}}
	{return: #}
}
*/
LMI_INLINE_DECLARATION LmiBool LmiCameraControlCapabilitiesHasPhotoCapture(const LmiCameraControlCapabilities *caps);

/**
{function:
	{name: LmiCameraControlCapabilitiesHasAny}
	{parent: LmiCameraControlCapabilities}
	{description: #}
	{prototype: LmiBool LmiCameraControlCapabilitiesHasAny(const LmiCameraControlCapabilities *caps)}
	{parameter: {name: caps} {description: #}}
	{return: #}
}
*/
LmiBool LmiCameraControlCapabilitiesHasAny(const LmiCameraControlCapabilities *caps);

LMI_END_EXTERN_C

#if LMI_INLINE_NEED_HEADER_FILE_DEFINITIONS
#include <Lmi/Video/Common/LmiCameraControlInline.h>
#endif

#endif
