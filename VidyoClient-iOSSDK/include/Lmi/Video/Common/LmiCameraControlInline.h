/**
{file:
	{name: LmiCameraControlInline.h}
	{description: }
	{copyright:
		(c) 2006-2019 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
LMI_INLINE void LmiCameraControlParameterDestruct(LmiCameraControlParameter *p)
{
}

LMI_INLINE LmiCameraControlParameter *LmiCameraControlParameterAssign(LmiCameraControlParameter *p, const LmiCameraControlParameter *other)
{
	*p = *other;
	return p;
}

LMI_INLINE void LmiCameraControlParameterSetFit(LmiCameraControlParameter *p, LmiBool enable)
{
	p->hasFit = enable;
}

LMI_INLINE void LmiCameraControlParameterSetDrive(LmiCameraControlParameter *p, LmiBool enable)
{
	p->hasDrive = enable;
}

LMI_INLINE void LmiCameraControlParameterSetNudge(LmiCameraControlParameter *p, LmiBool enable)
{
	p->hasNudge = enable;
}

LMI_INLINE LmiBool LmiCameraControlParameterHasFit(const LmiCameraControlParameter *p)
{
	return p->hasFit;
}

LMI_INLINE LmiBool LmiCameraControlParameterHasDrive(const LmiCameraControlParameter *p)
{
	return p->hasDrive;
}

LMI_INLINE LmiBool LmiCameraControlParameterHasNudge(const LmiCameraControlParameter *p)
{
	return p->hasNudge;
}

LMI_INLINE LmiBool LmiCameraControlParameterHasAny(const LmiCameraControlParameter *p)
{
	return p->hasFit || p->hasDrive || p->hasNudge;
}

LMI_INLINE void LmiCameraControlCapabilitiesSetPanTiltCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p)
{
	LmiCameraControlParameterAssign(&caps->panTiltCaps, p);
}

LMI_INLINE void LmiCameraControlCapabilitiesSetZoomCaps(LmiCameraControlCapabilities *caps, const LmiCameraControlParameter *p)
{
	LmiCameraControlParameterAssign(&caps->zoomCaps, p);
}

LMI_INLINE void LmiCameraControlCapabilitiesSetPhotoCapture(LmiCameraControlCapabilities *caps, LmiBool enable)
{
	caps->hasPhotoCapture = enable;
}

LMI_INLINE const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetPanTiltCaps(const LmiCameraControlCapabilities *caps)
{
	return &caps->panTiltCaps;
}

LMI_INLINE const LmiCameraControlParameter *LmiCameraControlCapabilitiesGetZoomCaps(const LmiCameraControlCapabilities *caps)
{
	return &caps->zoomCaps;
}

LMI_INLINE LmiBool LmiCameraControlCapabilitiesHasPhotoCapture(const LmiCameraControlCapabilities *caps)
{
	return caps->hasPhotoCapture;
}

LMI_INLINE LmiBool LmiCameraControlCapabilitiesHasAny(const LmiCameraControlCapabilities *caps)
{
	return LmiCameraControlParameterHasAny(&caps->panTiltCaps) || LmiCameraControlParameterHasAny(&caps->zoomCaps) || caps->hasPhotoCapture;
}
