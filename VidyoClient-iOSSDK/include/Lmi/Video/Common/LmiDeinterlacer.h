/**
   {file: 
	{name: LmiDeinterlacer.h}
	{description: Defines the main structures and functions related to Deinterlacer.}
	{copyright:
		(c) 2010-2015 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.
	
		All rights reserved.
	
		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
   }
*/


#ifndef LMI_DEINTERLACER_H_
#define LMI_DEINTERLACER_H_


/**
   {package visibility="private":
     {name: Deinterlacer}
	 {parent: VideoCommon}
     {include: Lmi/Video/Common/LmiDeinterlacer.h}
	 {description: Defines the main structures and functions related to deinterlacer.}
   }
*/
#include <Lmi/Video/Common/LmiVideoFrame.h>

LMI_BEGIN_EXTERN_C

/** 
   {type visibility="private":
     {name: LmiDeinterlacer}
     {parent: VideoCommon}
     {include: Lmi/Video/Common/LmiDeinterlacer.h}
     {description: deinterlacer object.}
   }
*/
typedef struct {
    LmiInt				prevIdx;
    LmiVideoFrame		i420Buf[2];

	LmiUint8			maxNonBkgPixelsAllowedForBkgY;
	LmiUint16			lumaBlkThresh;
    LmiUint8			lumaThreshBkg;
    LmiUint8			lumaThreshNonBkg;
    LmiUint32			sadAllowedForBkgY;
	LmiUint8			temp[256];
} LmiDeinterlacer;

/**
   {function visibility="private":
     {name: LmiDeinterlacerConstruct}
	 {parent: LmiDeinterlacer}
     {description: Construct LmiDeinterlacer object.}
     {prototype: LmiDeinterlacer *LmiDeinterlacerConstruct(LmiDeinterlacer *x, LmiAllocator *a)}
     {parameter: {name: x} {description: Destination LmiDeinterlacer object.}}
     {parameter: {name: a} {description: LmiAllocator object.}}
	 {return: Constructed LmiDeinterlacer object.}
   }
*/
LmiDeinterlacer *LmiDeinterlacerConstruct(LmiDeinterlacer *x, LmiAllocator *a);

/**
   {function visibility="private":
     {name: LmiDeinterlacerConstructCopy}
	 {parent: LmiDeinterlacer}
     {description: ConstructCopy LmiDeinterlacer object.}
     {prototype: LmiDeinterlacer *LmiDeinterlacerConstructCopy(LmiDeinterlacer *x, const LmiDeinterlacer *y)}
     {parameter: {name: x} {description: Destination LmiDeinterlacer object.}}
     {parameter: {name: y} {description: Source LmiDeinterlacer object.}}
	 {return: Constructed LmiDeinterlacer object.}
   }
*/
LmiDeinterlacer *LmiDeinterlacerConstructCopy(LmiDeinterlacer *x, const LmiDeinterlacer *y);

/**
   {function visibility="private":
     {name: LmiDeinterlacerDestruct}
	 {parent: LmiDeinterlacer}
     {description: Destruct LmiDeinterlacer object.}
     {prototype: void LmiDeinterlacerDestruct(LmiDeinterlacer *x)}
     {parameter: {name: x} {description: LmiDeinterlacer object.}}
   }
*/
void LmiDeinterlacerDestruct(LmiDeinterlacer *x);

/**
   {function visibility="private":
     {name: LmiDeinterlacerAssign}
	 {parent: LmiDeinterlacer}
     {description: Assign LmiDeinterlacer object.}
     {prototype: LmiDeinterlacer *LmiDeinterlacerAssign(LmiDeinterlacer *x, const LmiDeinterlacer *y)}
     {parameter: {name: x} {description: Destination LmiDeinterlacer object.}}
     {parameter: {name: y} {description: Source LmiDeinterlacer object.}}
	 {return: Assigned LmiDeinterlacer object.}
   }
*/
LmiDeinterlacer *LmiDeinterlacerAssign(LmiDeinterlacer *x, const LmiDeinterlacer *y);

/**
   {function visibility="private":
     {name: LmiDeinterlacerDeinterlaceVideoFrame}
	 {parent: LmiDeinterlacer}
     {description: Filter a YUV 420 image.}
     {prototype: LmiBool LmiDeinterlacerDeinterlaceVideoFrame(LmiDeinterlacer *x, LmiVideoFrame *image)}
     {parameter: {name: x} {description: LmiDeinterlacer object.}}
     {parameter: {name: image} {description: I420 frame to be deinterlaced.}}
	 {return: LMI_TRUE when succeeded.}
   }
*/
LmiBool LmiDeinterlacerDeinterlaceVideoFrame(LmiDeinterlacer *x, LmiVideoFrame *image);

LMI_END_EXTERN_C

#endif // LMI_DEINTERLACER_H_
