/**
{file: 
	{name: LmiDistortion.h}
	{description: Definition of distortion function setup.}
	{copyright:
		(c) 2013-2015 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
											***** CONFIDENTIAL *****
	}
}
*/


/**
{package visibility="private":
	{name: LmiDistortion}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiDistortion.h}
	{description: Definition of distortion functions.}
}
*/

#ifndef LMI_DISTORTION_H_
#define LMI_DISTORTION_H_

#include <Lmi/Utils/LmiTypes.h>

#define LMI_DISTORTIONTYPE_COUNT        4
#define LMI_DISTORTIONFUNCTION_MAXSIZE  7

typedef LmiUint (*LmiDistortionFunction)(const LmiUint8 *src0, LmiUint pitch0, const LmiUint8 *src1, LmiUint pitch1);
extern LmiDistortionFunction LmiDistortion[LMI_DISTORTIONTYPE_COUNT][LMI_DISTORTIONFUNCTION_MAXSIZE][LMI_DISTORTIONFUNCTION_MAXSIZE];

#if 0
typedef enum
{
	LMI_DISTORTIONTYPE_SAD     = 0,
	LMI_DISTORTIONTYPE_SSD     = 1,
	LMI_DISTORTIONTYPE_HAD     = 2,
	LMI_DISTORTIONTYPE_FastSAD = 3,
} LmiDistortionType;
#endif

void LmiDistortionInitialize(void);

LmiUint LmiDistortionScaleChroma(LmiUint dist, LmiUint8 qpy, LmiUint8 qpc);

#endif // LMI_DISTORTION_H_

