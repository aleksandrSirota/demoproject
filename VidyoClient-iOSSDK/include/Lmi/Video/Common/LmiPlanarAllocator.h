/** {file:
    {name: LmiPlanarAllocator.h}
    {description: Interface to planar (non-contiguous rectangular memory region) allocators. }
    {copyright:
    	(c) 2007-2012 Vidyo, Inc.,
    	433 Hackensack Avenue,
    	Hackensack, NJ  07601.
    
    	All rights reserved.
    
    	The information contained herein is proprietary to Vidyo, Inc.
    	and shall not be reproduced, copied (in whole or in part), adapted,
    	modified, disseminated, transmitted, transcribed, stored in a retrieval
    	system, or translated into any language in any form by any means
    	without the express written consent of Vidyo, Inc.
    	                  ***** CONFIDENTIAL *****
    }
    }
*/

#ifndef LMI_PLANARALLOCATOR_H_
#define LMI_PLANARALLOCATOR_H_

#include <Lmi/Utils/LmiTypes.h>
#include <Lmi/Utils/LmiAllocator.h>

LMI_BEGIN_EXTERN_C

/* Forward declaration; the callbacks and the planar allocator mutually depend on
 * each other. */

/**
{type visibility="public":
	{name: LmiPlanarAllocator}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiPlanarAllocator.h}
	{description: A Planar Allocator. - To be implemented; currently same as the LmiMallocAllocator.}
}
*/
typedef LmiAllocator LmiPlanarAllocator;

//typedef struct LmiPlanarAllocatorS_ LmiPlanarAllocator;
#define LmiPlanarAllocatorAllocate(a_, s_) LmiAllocatorAllocate((LmiAllocator *)a_, s_)
#define LmiPlanarAllocatorDeallocate(a_, b_, s_) LmiAllocatorDeallocate((LmiAllocator *)a_, b_, s_)


/**
   {function:
     {name: LmiPlanarAllocatorGetDefault}
     {parent: LmiPlanarAllocator}
     {description: Get the default PlanarAllocator.}
     {prototype: LmiPlanarAllocator* LmiPlanarAllocatorGetDefault(void)}
	 {return: The default PlanarAllocator.}
     {note: currently just a LmiMallocAllocator.}
   }
*/
LmiPlanarAllocator* LmiPlanarAllocatorGetDefault(void);	

LMI_END_EXTERN_C

#endif /* LMI_PLANARALLOCATOR_H_ */
