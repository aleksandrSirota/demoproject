/**
   {file: 
	{name: LmiVga2Usb.h}
	{description: Defines the main structures and functions related to VGA2USB input processing.}
	{copyright:
		(c) 2010-2015 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.
	
		All rights reserved.
	
		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
   }
*/


#ifndef LMI_VGA2USBPROCESS_H_
#define LMI_VGA2USBPROCESS_H_


/**
   {package visibility="private":
     {name: Vga2UsbProcess}
	 {parent: VideoCommon}
     {include: Lmi/Video/Common/LmiVga2Usb.h}
	 {description: Defines the main structures and functions related to VGA2USB input processing.}
   }
*/
#include <Lmi/Video/Common/LmiVideoFrame.h>

LMI_BEGIN_EXTERN_C

/**
{type visibility="private":
	{name: LmiVga2UsbDetectionHistory}
	{parent: Vga2UsbProcess}
	{description: Motion detection history object for VGA2Vga2UsbUSB algorithm.}
}
*/
typedef struct {
    LmiUint8 *row[2];  //must be padded with 1 extra pel on left and right
    LmiUint currRow;
    LmiUint prevRow;
    LmiUint currMbIndex;
} LmiVga2UsbDetectionHistory;

/** 
   {type visibility="private":
     {name: LmiVga2UsbProcess}
     {parent: Vga2UsbProcess}
     {description: Vga2Usb Preprocessor object.}
   }
*/
typedef struct {
    LmiUint width;
    LmiUint height;
    LmiUint pitch;

    LmiUint8 *ref[2];
    int refIndex;
    int offset[16];
    int firstFrame;
    LmiVga2UsbDetectionHistory dh;
    LmiAllocator *alloc;
} LmiVga2UsbProcess;

/**
   {function visibility="private":
     {name: LmiVga2UsbProcessConstruct}
	 {parent: LmiVga2UsbProcess}
     {description: Construct LmiVga2UsbProcess object.}
     {prototype: LmiVga2UsbProcess *LmiVga2UsbProcessConstruct(LmiVga2UsbProcess *x, LmiUint width, LmiUint height, LmiUint pitch, LmiAllocator *a)}
     {parameter: {name: x} {description: Destination LmiVga2UsbProcess object.}}
     {parameter: {name: width} {description: Width of frames to be processed.}}
     {parameter: {name: height} {description: Height of frames to be processed.}}
     {parameter: {name: pitch} {description: Pitch of frames to be processed.}}
     {parameter: {name: a} {description: LmiAllocator object.}}
	 {return: Constructed LmiVga2UsbProcess object.}
   }
*/
LmiVga2UsbProcess *LmiVga2UsbProcessConstruct(LmiVga2UsbProcess *p, LmiUint width, LmiUint height, LmiUint pitch, LmiAllocator *a);


/**
   {function visibility="private":
     {name: LmiVga2UsbProcessConstructCopy}
	 {parent: LmiVga2UsbProcess}
     {description: ConstructCopy LmiVga2UsbProcess object.}
     {prototype: LmiVga2UsbProcess *LmiVga2UsbProcessConstructCopy(LmiVga2UsbProcess *x, const LmiVga2UsbProcess *y)}
     {parameter: {name: x} {description: Destination LmiVga2UsbProcess object.}}
     {parameter: {name: y} {description: Source LmiVga2UsbProcess object.}}
	 {return: Constructed LmiVga2UsbProcess object.}
   }
*/
LmiVga2UsbProcess *LmiVga2UsbProcessConstructCopy(LmiVga2UsbProcess *x, const LmiVga2UsbProcess *y);

/**
   {function visibility="private":
     {name: LmiVga2UsbProcessDestruct}
	 {parent: LmiVga2UsbProcess}
     {description: Destruct LmiVga2UsbProcess object.}
     {prototype: void LmiVga2UsbProcessDestruct(LmiVga2UsbProcess *x)}
     {parameter: {name: x} {description: LmiVga2UsbProcess object.}}
   }
*/
void LmiVga2UsbProcessDestruct(LmiVga2UsbProcess *x);

/**
   {function visibility="private":
     {name: LmiVga2UsbProcessVideoFrame}
	 {parent: LmiVga2UsbProcess}
     {description: Process YUV 420 frame with Vga2Usb preprocessor.}
     {prototype: void LmiVga2UsbProcessVideoFrame(LmiVga2UsbProcess *p, LmiVideoFrame *x, LmiUint width, LmiUint height, LmiUint pitch)}
     {parameter: {name: p} {description: LmiVga2UsbProcess object.}}
     {parameter: {name: x} {description: LmiVideoFrame object. Must be I420 format.}}
     {parameter: {name: width} {description: Width of frame.}}
     {parameter: {name: height} {description: Height of frame.}}
     {parameter: {name: pitch} {description: Pitch of frame.}}
   }
*/
void LmiVga2UsbProcessVideoFrame(LmiVga2UsbProcess *p, LmiVideoFrame *x, LmiUint width, LmiUint height, LmiUint pitch);

LMI_END_EXTERN_C

#endif // LMI_DEINTERLACER_H_
