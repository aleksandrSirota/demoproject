#ifndef _LMIVIDEOCOMMONCONFIG_H_
#define _LMIVIDEOCOMMONCONFIG_H_

/*
{file:
	{name: LmiVideoCommonConfig.h}
	{description: Syntax validation and parser for LmiVideoCommonConfig.xsd schema.}
	{copyright:
		(c) 2011-2013 Vidyo, Inc.,
			433 Hackensack Avenue,
			Hackensack, NJ  07601.

			All rights reserved.

			The information contained herein is proprietary to Vidyo, Inc.
			and shall not be reproduced, copied (in whole or in part), adapted,
			modified, disseminated, transmitted, transcribed, stored in a retrieval
			system, or translated into any language in any form by any means
			without the express written consent of Vidyo, Inc.
							***** CONFIDENTIAL *****
	}
}
*/

#include "LmiVideoCommonConfigAuto.h"

/*****************/
/* Dynamic range */
/*****************/

LmiDynamicRange *LmiDynamicRangeConstructDefault_(LmiDynamicRange *x, LmiAllocator *a);


#define LMI_CODEC_NumAbsDiffBin (22)

/**
{function visibility="private":
	{name: LmiTemporalFilterConstructDefault_}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Construct LmiTemporalFilter data.}
	{prototype: LmiTemporalFilter *LmiTemporalFilterConstructDefault_(LmiTemporalFilter *x, LmiAllocator* a)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: a} {description: Allocator to use for creating objects inside this object.}}
	{return: Constructed LmiTemporalFilter data.}
}
*/
LmiTemporalFilter *LmiTemporalFilterConstructDefault_(LmiTemporalFilter *x, LmiAllocator* a);

/**
{function visibility="internal":
	{name: LmiTemporalFilterSetBlockThresh}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Set luma & chroma block threshold values.}
	{prototype: LmiTemporalFilterStatus LmiTemporalFilterSetBlockThresh(LmiTemporalFilter *x, LmiUint16 lumaBlkThresh, LmiUint16 chromaBlkThresh)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: lumaBlkThresh} {description: Assigned temporal filter luma block threshold.}}
	{parameter: {name: chromaBlkThresh} {description: Assigned temporal filter chroma block threshold.}}
	{return: LmiTemporalFilterStatus.}
}
*/
LmiTemporalFilterStatus LmiTemporalFilterSetBlockThresh(LmiTemporalFilter *x, LmiUint16 lumaBlkThresh, LmiUint16 chromaBlkThresh);

/**
{function visibility="private":
	{name: LmiTemporalFilterGetThresholdBackground}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Get luma & chroma background pixel threshold values.}
	{prototype: void LmiTemporalFilterGetThresholdBackground(const LmiTemporalFilter *x, LmiUint8 *lumaThreshBkg, LmiUint8 *chromaThreshBkg)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: lumaThreshBkg} {description: Assigned temporal filter luma threshold for background pixels.}}
	{parameter: {name: chromaThreshBkg} {description: Assigned temporal filter chroma threshold for background pixels.}}
}
*/
void LmiTemporalFilterGetThresholdBackground(const LmiTemporalFilter *x, LmiUint8 *lumaThreshBkg, LmiUint8 *chromaThreshBkg);

/**
{function visibility="private":
	{name: LmiTemporalFilterSetThresholdBackground}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Set luma & chroma background pixel threshold values.}
	{prototype: LmiTemporalFilterStatus LmiTemporalFilterSetThresholdBackground(LmiTemporalFilter *x, LmiUint8 lumaThreshBkg, LmiUint8 chromaThreshBkg)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: lumaThreshBkg} {description: Assigned temporal filter luma threshold for background pixels.}}
	{parameter: {name: chromaThreshBkg} {description: Assigned temporal filter chroma threshold for background pixels.}}
	{return: LmiTemporalFilterStatus.}
}
*/
LmiTemporalFilterStatus LmiTemporalFilterSetThresholdBackground(LmiTemporalFilter *x, LmiUint8 lumaThreshBkg, LmiUint8 chromaThreshBkg);

/**
{function visibility="private":
	{name: LmiTemporalFilterGetThresholdNonBackground}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Get luma & chroma non-background pixel threshold values.}
	{prototype: void LmiTemporalFilterGetThresholdNonBackground(const LmiTemporalFilter *x, LmiUint8 *lumaThreshNonBkg, LmiUint8 *chromaThreshNonBkg)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: lumaThreshNonBkg} {description: Assigned temporal filter luma threshold for non-background pixels.}}
	{parameter: {name: chromaThreshNonBkg} {description: Assigned temporal filter chroma threshold for non-background pixels.}}
}
*/
void LmiTemporalFilterGetThresholdNonBackground(const LmiTemporalFilter *x, LmiUint8 *lumaThreshNonBkg, LmiUint8 *chromaThreshNonBkg);

/**
{function visibility="private":
	{name: LmiTemporalFilterSetThresholdNonBackground}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Set luma & chroma non-background pixel threshold values.}
	{prototype: LmiTemporalFilterStatus LmiTemporalFilterSetThresholdNonBackground(LmiTemporalFilter *x, LmiUint8 lumaThreshNonBkg, LmiUint8 chromaThreshNonBkg)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: lumaThreshNonBkg} {description: Assigned temporal filter luma threshold for non-background pixels.}}
	{parameter: {name: chromaThreshNonBkg} {description: Assigned temporal filter chroma threshold for non-background pixels.}}
	{return: LmiTemporalFilterStatus.}
}
*/
LmiTemporalFilterStatus LmiTemporalFilterSetThresholdNonBackground(LmiTemporalFilter *x, LmiUint8 lumaThreshNonBkg, LmiUint8 chromaThreshNonBkg);

/**
{function visibility="private":
	{name: LmiTemporalFilterSetBackgroundRequirement}
	{parent: VideoCommon}
	{include: Lmi/Video/Common/LmiVideoCommonConfig.h}
	{description: Set background requirement.  Set sadAllowedForBkg to 384 for slower, but better AVC IPPP encoding}
	{prototype: LmiTemporalFilterStatus LmiTemporalFilterSetBackgroundRequirement(LmiTemporalFilter *x, LmiUint32 sadAllowedForBkgY, LmiUint8 maxNonBkgPixelsAllowedForBkgY, LmiUint8 maxNonBkgPixelsAllowedForBkgCb, LmiUint8 maxNonBkgPixelsAllowedForBkgCr)}
	{parameter: {name: x} {description: Destination LmiTemporalFilter data.}}
	{parameter: {name: sadAllowedForBkgY} {description: Luma SAD value for background; set this to 0 to skip background detection.}}
	{parameter: {name: maxNonBkgPixelsAllowedForBkgY} {description: Maximum number of non-background Y pixels allowed for a background.}}
	{parameter: {name: maxNonBkgPixelsAllowedForBkgCb} {description: Maximum number of non-background Cb pixels allowed for a background.}}
	{parameter: {name: maxNonBkgPixelsAllowedForBkgCr} {description: Maximum number of non-background Cr pixels allowed for a background.}}
	{return: LmiTemporalFilterStatus.}
}
*/
LmiTemporalFilterStatus LmiTemporalFilterSetBackgroundRequirement(LmiTemporalFilter *x, LmiUint32 sadAllowedForBkgY, LmiUint8 maxNonBkgPixelsAllowedForBkgY, LmiUint8 maxNonBkgPixelsAllowedForBkgCb, LmiUint8 maxNonBkgPixelsAllowedForBkgCr);

void LmiTemporalFilterResetThreshold(LmiTemporalFilter *x);


#endif /* _LMIVIDEOCOMMONCONFIG.H_ */
