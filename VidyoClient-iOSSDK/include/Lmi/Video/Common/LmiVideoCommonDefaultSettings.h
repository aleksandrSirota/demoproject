/**
{file: 
	{name: LmiVideoCommonDefaultSettings.h}
	{description: Defines the default values for video common lib.}
	{copyright:
		(c) 2007-2013 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
											***** CONFIDENTIAL *****
	}
}
*/

#ifndef LMI_VIDEO_COMMON_DEFAULT_SETTINGS_H_
#define LMI_VIDEO_COMMON_DEFAULT_SETTINGS_H_

LMI_BEGIN_EXTERN_C

#define LMI_VIDEO_COMMON_DEFAULT_MaxSLyrs                   128
#define LMI_VIDEO_COMMON_DEFAULT_MaxTLyrs                   8

/* Temporal filter */
#define LMI_VIDEO_COMMON_DEFAULT_TFilterIsOn														LMI_TRUE
#define LMI_VIDEO_COMMON_DEFAULT_TFilterLumaThresh											7
#define LMI_VIDEO_COMMON_DEFAULT_TFilterChromaThresh										6
#define LMI_VIDEO_COMMON_DEFAULT_TFilterLumaBlkThresh                   256
#define LMI_VIDEO_COMMON_DEFAULT_TFilterChromaBlkThresh                 64
#define LMI_VIDEO_COMMON_DEFAULT_TFilterLumaThreshBkg                   10
#define LMI_VIDEO_COMMON_DEFAULT_TFilterChromaThreshBkg                 10
#define LMI_VIDEO_COMMON_DEFAULT_TFilterLumaThreshNonBkg                2
#define LMI_VIDEO_COMMON_DEFAULT_TFilterChromaThreshNonBkg							2
#define LMI_VIDEO_COMMON_DEFAULT_TFilterSadAllowedForBkgY								1024
#define LMI_VIDEO_COMMON_DEFAULT_TFilterMaxNonBkgPixelsAllowedForBkgY		1 // 4
#define LMI_VIDEO_COMMON_DEFAULT_TFilterMaxNonBkgPixelsAllowedForBkgCb	1 // 2
#define LMI_VIDEO_COMMON_DEFAULT_TFilterMaxNonBkgPixelsAllowedForBkgCr	1 // 2
#define LMI_VIDEO_COMMON_DEFAULT_TFilterSumIntraFrameDiffThresh					64
#define LMI_VIDEO_COMMON_DEFAULT_StrongBkgTFilterThresh									20


/* DR adjustment - turned off by default */
#define LMI_VIDEO_COMMON_DEFAULT_DR_OffsetY                 0
#define LMI_VIDEO_COMMON_DEFAULT_DR_Base128SlopeY           0
#define LMI_VIDEO_COMMON_DEFAULT_DR_MinY                    0
#define LMI_VIDEO_COMMON_DEFAULT_DR_MaxY                    0

/* Bkg Detection */
#define LMI_VIDEO_COMMON_DEFAULT_BkgDetectionThreshBkg									10
#define LMI_VIDEO_COMMON_DEFAULT_BkgDetectionThreshNonBkg								2
#define LMI_VIDEO_COMMON_DEFAULT_BkgDetectionLumaBlkThresh							128

LMI_END_EXTERN_C

#endif // LMI_VIDEO_COMMON_DEFAULT_SETTINGS_H_
