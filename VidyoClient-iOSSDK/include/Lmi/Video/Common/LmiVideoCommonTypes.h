/**
{file: 
	{name: LmiVideoCommonTypes.h}
	{description: Definition of data types used in all Video stuff.}
	{copyright:
		(c) 2013 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
											***** CONFIDENTIAL *****
	}
}
*/

#ifndef LMI_VIDEOCOMMONTYPES_H_
#define LMI_VIDEOCOMMONTYPES_H_

LMI_BEGIN_EXTERN_C

typedef struct
{
	LmiUint16 sad0;
	LmiUint16 sad1;
} LmiDualSad_;

typedef struct
{
	LmiUint16 sad0;
	LmiUint16 sad1;
	LmiUint16 sad2;
	LmiUint16 sad3;
} LmiQuadSad_;

LMI_END_EXTERN_C

#endif // LMI_VIDEOCOMMONTYPES_H_
