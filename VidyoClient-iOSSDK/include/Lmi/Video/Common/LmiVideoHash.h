/**
{file:
	{name: LmiVideoHash.h}
	{description: Defines LmiVideoHash functions.}
	{copyright:
		(c) 2013 Vidyo, Inc.,
		433 Hackensack Avenue,
		Hackensack, NJ  07601.

		All rights reserved.

		The information contained herein is proprietary to Vidyo, Inc.
		and shall not be reproduced, copied (in whole or in part), adapted,
		modified, disseminated, transmitted, transcribed, stored in a retrieval
		system, or translated into any language in any form by any means
		without the express written consent of Vidyo, Inc.
		                  ***** CONFIDENTIAL *****
	}
}
*/
#ifndef LMI_VIDEOHASH_H
#define LMI_VIDEOHASH_H

#include <Lmi/Utils/LmiTypes.h>


LMI_BEGIN_EXTERN_C


/**
	{function visibility="private":
		{name: Lmi2DHash}
		{parent: VideoCommon}
		{include: Lmi/Video/Common/LmiVideoHash.h}
		{library: LmiVideoCommon}
		{description: Provide Md5, CRC or checksum hashing of 2D buffer.}
		{prototype: LmiBool Lmi2DHash( LmiUint8 *output, LmiUint *outputLen, LmiUint8 *input, 
					LmiUint inputWidth, LmiUint inputHeight, LmiUint inputStride, 
					LmiUint hashMode)}
		{parameter:
			{name: output}
			{description: The output tag buffer.}}
		{parameter:
			{name: outputLen}
			{description: Pointer to the output tag buffer length.}}
		{parameter:
			{name: input}
			{description: The input buffer.}}
		{parameter:
			{name: inputWidth}
			{description: The length of line of data to process.}}
		{parameter:
			{name: inputHeight}
			{description: The number of input data lines to process.}}
		{parameter:
			{name: inputStride}
			{description: The inputWidth plus gap between data lines.}}
		{parameter:
			{name: hashMode}
			{description: Mode: 0=Md5, 1=CRC, 2=checksum.}}
		{return: LMI_TRUE on success, else LMI_FALSE.}
	}
*/
LmiBool Lmi2DHash(LmiUint8 *output, LmiUint *outputLen, LmiUint8 *input, LmiUint inputWidth, LmiUint inputHeight, LmiUint inputStride, LmiUint hashMode);


LMI_END_EXTERN_C

#endif /* LMI_VIDEOHASH_H */

